<?php
session_start();

if (isset($_REQUEST["status"]) && "starting" == $_REQUEST["status"]) {
    if(isset($_REQUEST["countPieces"]) && $_REQUEST["fileName"] && $_REQUEST["crc32"]) {
        $_SESSION["countPieces"] = intval($_REQUEST["countPieces"]);

        $_SESSION["userFileName"] = $_REQUEST["fileName"]; // TODO: sanitize filename

        if (isset($_SESSION["undoneFiles"][$_SESSION["userFileName"]])) {
            echo json_encode($_SESSION["undoneFiles"][$_SESSION["userFileName"]]);
            http_response_code(206);
            exit;
        }

        $_SESSION["undoneFiles"][$_SESSION["userFileName"]] = [];

        echo "OK";
        exit;
    }
}

if (isset($_REQUEST["status"]) && "transferring" == $_REQUEST["status"]) {
    if(isset($_FILES["myFile"]) && isset($_REQUEST["chunkNumber"])) {
        $chunkNumber = intval($_REQUEST["chunkNumber"]);
        echo $chunkNumber;
        if ($_FILES['myFile']['tmp_name']) {

            if (move_uploaded_file($_FILES['myFile']['tmp_name'], $_SESSION["userFileName"] . "_" . $chunkNumber)) {
                if (!isset($_SESSION["undoneFiles"])) {
                    $_SESSION["undoneFiles"] = [];
                    if (!$_SESSION["undoneFiles"][$_SESSION["userFileName"]]) {
                        $_SESSION["undoneFiles"][$_SESSION["userFileName"]] = [];
                    }
                }
                $_SESSION["undoneFiles"][$_SESSION["userFileName"]][$chunkNumber] = true;
                echo "OK";

                exit;
            }
        }
    }
    http_response_code(500);
}

if (isset($_REQUEST["status"]) && "done" == $_REQUEST["status"]) {

    if(count($_SESSION["undoneFiles"][$_SESSION["userFileName"]]) == $_SESSION["countPieces"] ) {
        function catFiles($outputPath) {
            $dest = fopen($outputPath,"a");
            for ($i=0; $i < $_SESSION["countPieces"]; $i++) {
                $f = $_SESSION["userFileName"] . "_" . $i;
                $FH = fopen($f,"r");
                $line = fgets($FH);
                while ($line !== false) {
                    fputs($dest,$line);
                    $line = fgets($FH);
                }
                fclose($FH);
                unlink($f);
            }
            fclose($dest);

            echo '/' . $outputPath;

            //unset($_SESSION["undoneFiles"][$_SESSION["userFileName"]]);

            exit;
        }

        function createZip($filename) {
            $zip = new ZipArchive;
            if ($zip->open($filename . ".zip", ZipArchive::CREATE) === TRUE) {
                $zip->addFile($filename, $filename);
                $zip->close();

                echo '/' . $filename . ".zip";

                unset($_SESSION["undoneFiles"][$_SESSION["userFileName"]]);
                exit;
            }
        }

        if (file_exists($_SESSION["userFileName"])) {
            echo '/' . $_SESSION["userFileName"];
            exit;
        }

        catFiles($_SESSION["userFileName"]);


        //createZip($_SESSION["userFileName"]);


    }
    http_response_code(500);
}



